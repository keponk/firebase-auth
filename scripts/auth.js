// add admin cloud function
const adminForm = document.querySelector('.admin-actions');
adminForm.addEventListener('submit', e => {
  e.preventDefault();
  const adminEmail = document.querySelector('#admin-email').value;
  const addAdminRole = functions.httpsCallable('addAdminRole');
  addAdminRole({ email: adminEmail }).then(result => {
    console.log(result);
  });
});

// auth status change listener
auth.onAuthStateChanged(user => {
  //console.log(user);
  if (user) {
    user.getIdTokenResult().then(idTokenResult => {
      user.admin = idTokenResult.claims.admin;
      setupUi(user);
    });

    // get firestore data
    db.collection('guides').onSnapshot(
      snapshot => {
        //console.log(snapshot.docs);
        setupGuides(snapshot.docs);
      },
      error => {
        console.error(error);
      }
    );
  } else {
    setupGuides([]);
    setupUi();
  }
});

// handle guide creation
const createGuideForm = document.querySelector('#create-form');
createGuideForm.addEventListener('submit', e => {
  e.preventDefault();
  db.collection('guides')
    .add({
      title: createGuideForm['title'].value,
      guideContent: createGuideForm['content'].value
    })
    .then(() => {
      console.log('Document successfully written!');
      // close modal
      const modal = document.querySelector('#modal-create');
      M.Modal.getInstance(modal).close();
      createGuideForm.reset();
    })
    .catch(error => {
      console.error('Error writing document: ', error.message);
    });
});

// sign up
const signupForm = document.querySelector('#signup-form');
signupForm.addEventListener('submit', e => {
  e.preventDefault();

  // get user info
  const email = signupForm['signup-email'].value;
  const password = signupForm['signup-password'].value;

  // sign up the user
  auth
    .createUserWithEmailAndPassword(email, password)
    .then(cred => {
      return db
        .collection('users')
        .doc(cred.user.uid)
        .set({
          bio: signupForm['signup-bio'].value
        });
    })
    .then(() => {
      const modal = document.querySelector('#modal-signup');
      M.Modal.getInstance(modal).close();
      signupForm.reset();
      signupForm.querySelector('.error').innerHTML = '';
    })
    .catch(error => {
      signupForm.querySelector('.error').innerHTML = error.message;
    });
});

//logout
const logoutForm = document.querySelector('#logout');
logout.addEventListener('click', e => {
  e.preventDefault();
  auth.signOut().then(() => {}, function(error) {
    // An error happened.
    console.error(error);
  });
});

// log in
const loginForm = document.querySelector('#login-form');
loginForm.addEventListener('submit', e => {
  e.preventDefault();

  // get user info
  const email = loginForm['login-email'].value;
  const password = loginForm['login-password'].value;

  // log in user
  auth
    .signInWithEmailAndPassword(email, password)
    .then(cred => {
      //console.log(cred.user);
      // close login modal and reset form
      const modal = document.querySelector('#modal-login');
      M.Modal.getInstance(modal).close();
      loginForm.reset();
      loginForm.querySelector('.error').innerHTML = '';
    })
    .catch(function(error) {
      // Handle Errors here.
      loginForm.querySelector('.error').innerHTML = error.message;
      console.error('Code: ' + errorCode + ': ' + errorMessage);
    });
});

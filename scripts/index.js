const guideList = document.querySelector('.guides');
const loggedOutLinks = document.querySelectorAll('.logged-out');
const loggedInLinks = document.querySelectorAll('.logged-in');
const accountDetails = document.querySelector('.account-details');
const accountBio = document.querySelector('.account-bio');
const adminItems = document.querySelectorAll('.admin');

const setupUi = user => {
  if (user) {
    if (user.admin) {
      adminItems.forEach(item => (item.style.display = 'block'));
    }
    // fil in account info
    db.collection('users')
      .doc(user.uid)
      .get()
      .then(doc => {
        const html = `<div>Logged in as ${user.email}</div>
      <div>${doc.data().bio}</div>
      <div class='pink-text'>${user.admin ? 'Admin' : ''}</div> `;
        accountDetails.innerHTML = html;
      });

    // toogle ui elements
    loggedInLinks.forEach(element => {
      element.style.display = 'block';
    });
    loggedOutLinks.forEach(element => {
      element.style.display = 'none';
    });
  } else {
    adminItems.forEach(item => (item.style.display = 'none'));
    accountDetails.innerHTML = '';
    loggedInLinks.forEach(element => {
      element.style.display = 'none';
    });
    loggedOutLinks.forEach(element => {
      element.style.display = 'block';
    });
  }
};

// set up guides
const setupGuides = docsArray => {
  if (docsArray.length) {
    let html = '';
    docsArray.forEach(doc => {
      const guide = doc.data();
      //console.log(guide);
      const li = `<li>
      <div class="collapsible-header grey lighten-4">${guide.title}</div>
      <div class="collapsible-body white">${guide.guideContent}</div>
    </li>`;
      html += li;
    });
    guideList.innerHTML = html;
  } else {
    guideList.innerHTML =
      '<h5 class="center-align">Login to view the guides</h5>';
  }
};

// setup materialize components
document.addEventListener('DOMContentLoaded', function() {
  var modals = document.querySelectorAll('.modal');
  M.Modal.init(modals);

  var items = document.querySelectorAll('.collapsible');
  M.Collapsible.init(items);
});
